import java.util.Scanner;

public class kalkulator {
    public static void main (String[] args){
        do {
            Scanner input = new Scanner(System.in);
            int menu1;
            boolean menuutama = true;
            System.out.println("------------------------------------");
            System.out.println("Kalkulator Penghitung Luas dan Volum");
            System.out.println("------------------------------------");
            System.out.println("Menu\n1. Hitung Luas Bidang\n2. Hitung Volum\n0. Tutup Aplikasi");
            System.out.println("-------------------------------------");
            System.out.print("Masukkan pilihan: ");
            menu1 = input.nextInt();

            if (menu1 == 1) {
                do{
                    int pil1, menu2;
                    System.out.println("------------------------------------");
                    System.out.println("Pilih Bidang yang Akan Dihitung");
                    System.out.println("------------------------------------");
                    System.out.println("1. Persegi Panjang\n" +
                            "2. Persegi \n" +
                            "3. Segitiga\n" +
                            "4. Lingkaran\n" +
                            "0. Kembali ke menu sebelumnya");
                    System.out.println("------------------------------------");
                    System.out.print("Masukkan pilihan: ");
                    pil1 = input.nextInt();

                    switch (pil1) {
                        case 1:
                            int panjang, lebar, result;
                            System.out.println("------------------------------------");
                            System.out.println("Hitung Luas Persegi Panjang");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Panjang : ");
                            panjang = input.nextInt();
                            System.out.print("Masukkan Lebar   : ");
                            lebar = input.nextInt();
                            result = panjang * lebar;
                            System.out.println("Processing ...");
                            System.out.println("Luas dari Persegi Panjang adalah " + result + " cm^2");
                            break;
                        case 2:
                            int sisi;
                            System.out.println("------------------------------------");
                            System.out.println("Hitung Luas Persegi");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Sisi    : ");
                            sisi = input.nextInt();
                            result = sisi * sisi;
                            System.out.println("Processing ...");
                            System.out.println("Luas dari Persegi adalah " + result + " cm^2");
                            break;
                        case 3:
                            int alas, tinggi;
                            double results;
                            System.out.println("------------------------------------");
                            System.out.println("Hitung Luas Segitiga");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Alas     : ");
                            alas = input.nextInt();
                            System.out.print("Masukkan Tinggi   : ");
                            tinggi = input.nextInt();
                            System.out.println("Processing ...");
                            results = 0.5 * alas * tinggi;
                            System.out.println("Luas dari Segitiga adalah " + results + " cm^2");
                            break;
                        case 4:
                            double jari, result1;
                            System.out.println("------------------------------------");
                            System.out.println("Hitung Luas Lingkaran");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Jari-Jari  : ");
                            jari = input.nextInt();
                            result1 = 3.14 * jari * jari;
                            System.out.println("Processing ...");
                            System.out.println("Luas dari Lingkaran adalah " + result1 + " cm^2");
                            break;
                        case 0:
                            menuutama = false;
                            break;
                        default:
                            System.out.println("Maaf Pilihan Tidak Tersedia");
                    }
                }while(menuutama);

            } else if (menu1 == 2) {
                do {
                    int pil2, menu2;
                    System.out.println("------------------------------------");
                    System.out.println("Pilih Bidang yang Akan Dihitung");
                    System.out.println("------------------------------------");
                    System.out.println("1. Kubus\n" +
                            "2. Balok\n" +
                            "3. Tabung\n" +
                            "0. Kembali ke menu sebelumnya");
                    System.out.println("------------------------------------");
                    System.out.print("Masukkan pilihan: ");
                    pil2 = input.nextInt();
                    switch (pil2) {
                        case 1:
                            int sisi, result;
                            System.out.println("------------------------------------");
                            System.out.println("Hitung Volum Kubus");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Sisi   : ");
                            sisi = input.nextInt();
                            result = sisi * sisi * sisi;
                            System.out.println("Processing ...");
                            System.out.println("Volum dari Kubus adalah " + result + " cm^3");
                            continue;
                        case 2:
                            int panjang, lebar, tinggi;
                            System.out.println("------------------------------------");
                            System.out.println("Hitung Volum Balok");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Panjang : ");
                            panjang = input.nextInt();
                            System.out.print("Masukkan Lebar   : ");
                            lebar = input.nextInt();
                            System.out.print("Masukkan Tinggi  : ");
                            tinggi = input.nextInt();
                            result = panjang * lebar * tinggi;
                            System.out.println("Processing ...");
                            System.out.println("Volum dari Balok adalah " + result + " cm^3");
                            break;
                        case 3:
                            int jari;
                            double result1;
                            System.out.println("------------------------------------");
                            System.out.println("Hitung Volum Tabung");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Jari-Jari : ");
                            jari = input.nextInt();
                            System.out.print("Masukkan Tinggi    : ");
                            tinggi = input.nextInt();
                            result1 = 3.14 * tinggi * (jari * jari);
                            System.out.println("Processing ...");
                            System.out.println("Volum dari Tabung adalah " + result1 + "cm^3");
                            break;
                        case 0 :
                            menuutama = false;
                            break;
                        default:
                            System.out.println("Maaf Pilihan Tidak Tersedia");
                            break;
                    }
                }while (menuutama);
            } else if (menu1 == 0) {
                System.out.println("Terima Kasih Telah Menggunakan Aplikasi ini Bung :)");
                System.exit(0);
            } else
                System.out.println("Maaf Pilihan Tidak Tersedia");
        } while (true);




    }
}
